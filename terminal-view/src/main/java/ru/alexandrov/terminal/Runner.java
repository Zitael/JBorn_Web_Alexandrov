package ru.alexandrov.terminal;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.alexandrov.dao.MyException;
import ru.alexandrov.service.AuthorizationService;
import ru.alexandrov.service.BankAccountService;
import ru.alexandrov.service.ServiceConfiguration;
import ru.alexandrov.service.dto.BankAccountDTO;
import ru.alexandrov.service.dto.UserDTO;

import java.util.Scanner;

public class Runner {

    public static void main(String[] args) throws MyException {
        String login;
        String password;
        Scanner in = new Scanner(System.in);
        System.out.println("Введите логин:");
        login = in.next();
        System.out.println("Введите пароль:");
        password = in.next();
        ApplicationContext context = new AnnotationConfigApplicationContext(ServiceConfiguration.class);
        AuthorizationService authorizationService = context.getBean(AuthorizationService.class);

        UserDTO authorizedUser = authorizationService.authorization(login, password);
        if (authorizedUser != null) {
            System.out.println("Авторизация прошла успешно, " + authorizedUser.getName());
            BankAccountService bankAccountService = context.getBean(BankAccountService.class);
            BankAccountDTO bankAccount = bankAccountService.findBankAccountById(authorizedUser.getId());
            if (bankAccount != null) {
                System.out.println(bankAccount.getAccountName());
                System.out.println(bankAccount.getAmount());
            }
        } else {
            System.out.println("Неверный логин или пароль.");
        }
    }
}

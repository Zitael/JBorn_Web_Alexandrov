package ru.alexandrov.service;

import org.junit.Before;
import org.junit.Test;
import ru.alexandrov.dao.TransactionsDAO;
import ru.alexandrov.service.dto.TransactionDTO;
import ru.alexandrov.dao.entity.Transactions;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TransactionServiceTest {
    private TransactionsDAO transactionsDAO = mock(TransactionsDAO.class);
    private TransactionService subj = new TransactionService(transactionsDAO);

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void findTransactionsByIdWithSuccess() {
        Transactions transactions = new Transactions();
        when(transactionsDAO.findById(1)).thenReturn(transactions);
        TransactionDTO transactionDTO = subj.findTransactionsById(1);
        assertNotNull(transactionDTO);
    }
    @Test
    public void findTransactionsByIdWhenIdNotFound() {
        when(transactionsDAO.findById(1)).thenReturn(null);
        TransactionDTO transactionDTO = subj.findTransactionsById(1);
        assertNull(transactionDTO);
    }
}
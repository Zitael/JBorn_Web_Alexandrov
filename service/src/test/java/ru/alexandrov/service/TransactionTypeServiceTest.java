package ru.alexandrov.service;

import org.junit.Before;
import org.junit.Test;
import ru.alexandrov.dao.TransactionsTypesDAO;
import ru.alexandrov.service.dto.TransactionTypeDTO;
import ru.alexandrov.dao.entity.TransactionsTypes;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TransactionTypeServiceTest {
    private TransactionsTypesDAO transactionsTypesDAO = mock(TransactionsTypesDAO.class);
    private TransactionTypeService subj = new TransactionTypeService(transactionsTypesDAO);

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void getTransactionTypeWithSuccess() {
        TransactionsTypes transactionsTypes = new TransactionsTypes();
        when(transactionsTypesDAO.findById(1)).thenReturn(transactionsTypes);
        TransactionTypeDTO transactionTypeDTO = subj.findTransactionTypeById(1);
        assertNotNull(transactionTypeDTO);
    }
    @Test
    public void getTransactionTypeWhenIdNotFound() {
        when(transactionsTypesDAO.findById(1)).thenReturn(null);
        TransactionTypeDTO transactionTypeDTO = subj.findTransactionTypeById(1);
        assertNull(transactionTypeDTO);
    }
}
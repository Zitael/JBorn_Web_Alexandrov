package ru.alexandrov.service;

import org.junit.Before;
import org.junit.Test;
import ru.alexandrov.dao.UsersDAO;
import ru.alexandrov.service.dto.UserDTO;
import ru.alexandrov.dao.entity.Users;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class UserServiceTest {
    private UsersDAO usersDAO = mock(UsersDAO.class);
    private UserService subj = new UserService(usersDAO);

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void findUserByIdWithSuccess() {
        Users users = new Users();
        when(usersDAO.findById(1)).thenReturn(users);
        UserDTO userDTO = subj.findUserById(1);
        assertNotNull(userDTO);
    }
    @Test
    public void findUserByIdWhenIdNotFound() {
        when(usersDAO.findById(1)).thenReturn(null);
        UserDTO userDTO = subj.findUserById(1);
        assertNull(userDTO);
    }
}
package ru.alexandrov.service;

import org.junit.Before;
import org.junit.Test;
import ru.alexandrov.dao.BankAccountTypesDAO;
import ru.alexandrov.service.dto.BankAccountTypeDTO;
import ru.alexandrov.dao.entity.BankAccountTypes;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class BankAccountTypeServiceTest {
    private BankAccountTypesDAO bankAccountTypesDAO = mock(BankAccountTypesDAO.class);
    private BankAccountTypeService subj = new BankAccountTypeService(bankAccountTypesDAO);

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void findBankAccountTypeByIdWithSuccess() {
        BankAccountTypes bankAccountTypes = new BankAccountTypes();
        when(bankAccountTypesDAO.findById(1)).thenReturn(bankAccountTypes);
        BankAccountTypeDTO bankAccountTypeDTO = subj.findBankAccountTypeById(1);
        assertNotNull(bankAccountTypeDTO);
    }
    @Test
    public void findBankAccountTypeByIdWhenIdNotFound() {
        when(bankAccountTypesDAO.findById(1)).thenReturn(null);
        BankAccountTypeDTO bankAccountTypeDTO = subj.findBankAccountTypeById(1);
        assertNull(bankAccountTypeDTO);
    }
}
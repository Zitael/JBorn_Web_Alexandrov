package ru.alexandrov.service;

import org.junit.Before;
import org.junit.Test;
import ru.alexandrov.dao.UsersDAO;
import ru.alexandrov.dao.entity.Users;
import ru.alexandrov.service.dto.UserDTO;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class AuthorizationServiceTest {
    private UsersDAO usersDAO = mock(UsersDAO.class);
    private DigestService digestService = mock(DigestService.class);
    private UserService userService = mock(UserService.class);
    private AuthorizationService subj = new AuthorizationService(usersDAO, digestService, userService);

    @Before
    public void setUp() throws Exception {

    }

    @Test
    public void authorizationWithSuccessAuth() {
        Users users = new Users();
        users.setId(1);
        users.setPassword("123");
        when(usersDAO.findByEmail("log")).thenReturn(users);
        when(digestService.hash("pass")).thenReturn("123");
        UserDTO result = subj.authorization("log", "pass");
        assertNotNull(result);
        assertEquals(1, result.getId());
    }

    @Test
    public void authorizationWithUserNotFound() {
        when(usersDAO.findByEmail("log")).thenReturn(null);
        when(digestService.hash("pass")).thenReturn("123");
        UserDTO result = subj.authorization("log", "pass");
        assertNull(result);
    }

    @Test
    public void registration() {
        Users users = new Users();
        users.setId(1);
        users.setLogin("123");
        users.setPassword("123");
        when(digestService.hash("pass")).thenReturn("123");
        UserDTO result = subj.registration("log", "pass", "name");
        assertNotNull(result);
    }
}
package ru.alexandrov.service;

import org.junit.Before;
import org.junit.Test;
import ru.alexandrov.dao.TransactionsCategoryDAO;
import ru.alexandrov.service.dto.TransactionCategoryDTO;
import ru.alexandrov.dao.entity.TransactionsCategory;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TransactionCategoryServiceTest {
    private TransactionsCategoryDAO transactionsCategoryDAO = mock(TransactionsCategoryDAO.class);
    private TransactionCategoryService subj = new TransactionCategoryService(transactionsCategoryDAO);

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void findTransactionCategoryByIdWithSuccess() {
        TransactionsCategory transactionsCategory = new TransactionsCategory();
        when(transactionsCategoryDAO.findById(1)).thenReturn(transactionsCategory);
        TransactionCategoryDTO transactionCategoryDTO = subj.findTransactionCategoryById(1);
        assertNotNull(transactionCategoryDTO);
    }
    @Test
    public void findTransactionCategoryByIdWhenIdNotFound() {
        when(transactionsCategoryDAO.findById(1)).thenReturn(null);
        TransactionCategoryDTO transactionCategoryDTO = subj.findTransactionCategoryById(1);
        assertNull(transactionCategoryDTO);
    }
}
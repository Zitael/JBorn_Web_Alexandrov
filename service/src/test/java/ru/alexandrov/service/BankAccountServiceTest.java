package ru.alexandrov.service;

import org.junit.Before;
import org.junit.Test;
import ru.alexandrov.dao.BankAccountDAO;
import ru.alexandrov.service.dto.BankAccountDTO;
import ru.alexandrov.dao.entity.BankAccount;


import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class BankAccountServiceTest {
    private BankAccountDAO bankAccountDAO = mock(BankAccountDAO.class);
    private BankAccountService subj = new BankAccountService(bankAccountDAO);

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void findBankAccountByIdWithSuccess() {
        BankAccount bankAccount = new BankAccount();
        when(bankAccountDAO.findById(1)).thenReturn(bankAccount);
        BankAccountDTO bankAccountDTO = subj.findBankAccountById(1);
        assertNotNull(bankAccountDTO);
    }
    @Test
    public void findBankAccountByIdWhenIdNotFound() {
        when(bankAccountDAO.findById(1)).thenReturn(null);
        BankAccountDTO bankAccountDTO = subj.findBankAccountById(1);
        assertNull(bankAccountDTO);
    }
}
package ru.alexandrov.service.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@Accessors (chain = true)
public class BankAccountDTO {
    private BigDecimal amount;
    private String accountName;
    private int userId;
    private int accountTypeId;
}

package ru.alexandrov.service.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
public class UserDTO {
    private int id;
    private String login;
    private String name;
}

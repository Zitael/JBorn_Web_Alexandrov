package ru.alexandrov.service.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@Accessors(chain = true)
public class TransactionDTO {
    private int accountId;
    private int transactionTypeId;
    private BigDecimal transactionAmount;
}

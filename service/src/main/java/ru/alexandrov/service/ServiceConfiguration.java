package ru.alexandrov.service;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import ru.alexandrov.dao.JpaConfiguration;

@Configuration
@ComponentScan
@Import(JpaConfiguration.class)
public class ServiceConfiguration {
}
package ru.alexandrov.service;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.stereotype.Service;

@Service
public class DigestService {
    public String hash(String s) {
        return DigestUtils.md5Hex(s);
    }
}

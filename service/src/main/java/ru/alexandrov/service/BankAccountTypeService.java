package ru.alexandrov.service;

import org.springframework.stereotype.Service;
import ru.alexandrov.dao.BankAccountTypesDAO;
import ru.alexandrov.dao.MyException;
import ru.alexandrov.service.dto.BankAccountTypeDTO;
import ru.alexandrov.dao.entity.BankAccountTypes;

@Service
public class BankAccountTypeService {
    private final BankAccountTypesDAO bankAccountTypesDAO;

    public BankAccountTypeService(BankAccountTypesDAO bankAccountTypesDAO) {
        this.bankAccountTypesDAO = bankAccountTypesDAO;
    }

    public BankAccountTypeDTO findBankAccountTypeById(int id) throws MyException {
        BankAccountTypes bankAccountTypes = bankAccountTypesDAO.findById(id);
        if (bankAccountTypes != null) {
            BankAccountTypeDTO bankAccountTypeDTO = new BankAccountTypeDTO();
            bankAccountTypeDTO.setAccountTypeName(bankAccountTypes.getAccountTypeName());
            return bankAccountTypeDTO;
        }
        return null;
    }
}
package ru.alexandrov.service;

import org.springframework.stereotype.Service;
import ru.alexandrov.dao.BankAccountDAO;
import ru.alexandrov.dao.MyException;
import ru.alexandrov.dao.entity.BankAccount;
import ru.alexandrov.service.dto.BankAccountDTO;

import java.util.ArrayList;
import java.util.List;

@Service
public class BankAccountService {
    private final BankAccountDAO bankAccountDAO;

    public BankAccountService(BankAccountDAO bankAccountDAO) {
        this.bankAccountDAO = bankAccountDAO;
    }

    public BankAccountDTO findBankAccountById(int id) throws MyException {
        BankAccount bankAccount = bankAccountDAO.findById(id);
        if (bankAccount != null) {
            BankAccountDTO bankAccountDTO = new BankAccountDTO();
            bankAccountDTO.setAccountName(bankAccount.getAccountName());
            bankAccountDTO.setAccountTypeId(bankAccount.getBankAccountTypes().getId());
            bankAccountDTO.setAmount(bankAccount.getAmount());
            bankAccountDTO.setUserId(bankAccount.getUsers().getId());
            return bankAccountDTO;
        }
        return null;
    }
    public ArrayList<BankAccountDTO> findBankAccountsByUserId(int id) throws MyException {
        List<BankAccount> accountList = bankAccountDAO.findByUserId(id);
        ArrayList<BankAccountDTO> accountDTOS = new ArrayList<BankAccountDTO>();
        if (accountList != null) {
            for (BankAccount bankAccount: accountList) {
                BankAccountDTO bankAccountDTO = new BankAccountDTO();
                bankAccountDTO.setAccountName(bankAccount.getAccountName());
                bankAccountDTO.setAccountTypeId(bankAccount.getBankAccountTypes().getId());
                bankAccountDTO.setAmount(bankAccount.getAmount());
                bankAccountDTO.setUserId(bankAccount.getUsers().getId());
                accountDTOS.add(bankAccountDTO);
            }
        }
        return accountDTOS;
    }
}

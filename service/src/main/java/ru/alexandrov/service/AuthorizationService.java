package ru.alexandrov.service;

import org.springframework.stereotype.Service;
import ru.alexandrov.dao.MyException;
import ru.alexandrov.dao.UsersDAO;
import ru.alexandrov.service.dto.UserDTO;
import ru.alexandrov.dao.entity.Users;

@Service("AuthorizationService")
public class AuthorizationService {
    private final UsersDAO usersDAO;
    private final DigestService digestService;
    private final UserService userService;

    public AuthorizationService(UsersDAO usersDAO, DigestService digestservice, UserService userService) {
        this.usersDAO = usersDAO;
        this.digestService = digestservice;
        this.userService = userService;
    }

    public UserDTO authorization(String login, String password) throws MyException {
        Users user = usersDAO.findByEmail(login);
        if (user == null) {
            return null;
        }
        String passwordHash = digestService.hash(password);
        if (passwordHash.equals(user.getPassword())) {
            UserDTO userDTO = userService.findUserById(user.getId());
            if (userDTO != null) {
                userDTO.setName(user.getName());
                userDTO.setLogin(user.getLogin());
                userDTO.setId(user.getId());
                return userDTO;
            }
        }
        return null;
    }

    public UserDTO registration(String login, String password, String name) throws MyException {
        Users user = new Users();
        String passwordHash = digestService.hash(password);
        user.setLogin(login);
        user.setPassword(passwordHash);
        user.setName(name);
        usersDAO.insert(user);
        UserDTO userDTO = new UserDTO();
        userDTO.setId(user.getId());
        userDTO.setName(user.getName());
        userDTO.setLogin(user.getLogin());
        return userDTO;
    }
}
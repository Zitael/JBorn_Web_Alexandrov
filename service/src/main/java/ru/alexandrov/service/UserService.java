package ru.alexandrov.service;

import org.springframework.stereotype.Service;
import ru.alexandrov.dao.MyException;
import ru.alexandrov.dao.UsersDAO;
import ru.alexandrov.service.dto.UserDTO;
import ru.alexandrov.dao.entity.Users;

@Service
public class UserService {
    private final UsersDAO usersDAO;

    public UserService(UsersDAO usersDAO) {
        this.usersDAO = usersDAO;
    }

    public UserDTO findUserById(Integer id) throws MyException {
        Users users = usersDAO.findById(id);
        if (users != null) {
            UserDTO userDTO = new UserDTO();
            userDTO.setId(users.getId());
            userDTO.setLogin(users.getLogin());
            userDTO.setName(users.getName());
            return userDTO;
        }
        return null;
    }
}
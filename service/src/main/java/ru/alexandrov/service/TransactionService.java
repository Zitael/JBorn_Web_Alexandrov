package ru.alexandrov.service;

import org.springframework.stereotype.Service;
import ru.alexandrov.dao.MyException;
import ru.alexandrov.dao.TransactionsDAO;
import ru.alexandrov.service.dto.TransactionDTO;
import ru.alexandrov.dao.entity.Transactions;

@Service
public class TransactionService {
    private final TransactionsDAO transactionsDAO;

    public TransactionService(TransactionsDAO transactionsDAO) {
        this.transactionsDAO = transactionsDAO;
    }

    public TransactionDTO findTransactionsById(Integer id) throws MyException {
        Transactions transactions = transactionsDAO.findById(id);
        if (transactions != null) {
            TransactionDTO transactionDTO = new TransactionDTO();
            transactionDTO.setAccountId(transactions.getUsers().getId());
            transactionDTO.setTransactionTypeId(transactions.getTransactionsTypes().getId());
            transactionDTO.setTransactionAmount(transactions.getTransactionAmount());
            return transactionDTO;
        }
        return null;
    }
}
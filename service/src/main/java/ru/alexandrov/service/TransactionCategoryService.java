package ru.alexandrov.service;

import org.springframework.stereotype.Service;
import ru.alexandrov.dao.MyException;
import ru.alexandrov.dao.TransactionsCategoryDAO;
import ru.alexandrov.service.dto.TransactionCategoryDTO;
import ru.alexandrov.dao.entity.TransactionsCategory;

@Service
public class TransactionCategoryService {
    private final TransactionsCategoryDAO transactionsCategoryDAO;

    public TransactionCategoryService(TransactionsCategoryDAO transactionsCategoryDAO) {
        this.transactionsCategoryDAO = transactionsCategoryDAO;
    }

    public TransactionCategoryDTO findTransactionCategoryById(Integer id) throws MyException {
        TransactionsCategory transactionsCategory = transactionsCategoryDAO.findById(id);
        if (transactionsCategory != null) {
            TransactionCategoryDTO transactionCategoryDTO = new TransactionCategoryDTO();
            transactionCategoryDTO.setCategoryName(transactionsCategory.getCategoryName());
            return transactionCategoryDTO;
        }
        return null;
    }
}
package ru.alexandrov.service;

import org.springframework.stereotype.Service;
import ru.alexandrov.dao.MyException;
import ru.alexandrov.dao.TransactionsTypesDAO;
import ru.alexandrov.service.dto.TransactionTypeDTO;
import ru.alexandrov.dao.entity.TransactionsTypes;

@Service
public class TransactionTypeService {
    private final TransactionsTypesDAO transactionsTypesDAO;

    public TransactionTypeService(TransactionsTypesDAO transactionsTypesDAO) {
        this.transactionsTypesDAO = transactionsTypesDAO;
    }

    public TransactionTypeDTO findTransactionTypeById(Integer id) throws MyException {
        TransactionsTypes transactionsTypes = transactionsTypesDAO.findById(id);
        if (transactionsTypes != null) {
            TransactionTypeDTO transactionTypeDTO = new TransactionTypeDTO();
            transactionTypeDTO.setTransactionType(transactionsTypes.getTransactionType());
            transactionTypeDTO.setTransactionCategoryId(transactionsTypes.getTransactionsCategory().getId());
            return transactionTypeDTO;
        }
        return null;
    }
}
package ru.alexandrov.webview;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.alexandrov.webview.json.ControllerConfiguration;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class ContextListener implements ServletContextListener {
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        ServletContext servletContext = sce.getServletContext();
        ApplicationContext serviceApplicationContext = new AnnotationConfigApplicationContext(ControllerConfiguration.class);
        servletContext.setAttribute("springContext", serviceApplicationContext);
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
    }
}

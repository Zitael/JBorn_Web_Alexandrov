package ru.alexandrov.webview;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.ApplicationContext;
import ru.alexandrov.webview.json.ControllerPost;
import ru.alexandrov.webview.json.response.ErrorResponse;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class RegistrationServlet extends HttpServlet {
    private ObjectMapper om = new ObjectMapper();
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) {
        ApplicationContext serviceApplicationContext = ( ApplicationContext ) req.getServletContext().getAttribute("springContext");
        ControllerPost controller = ( ControllerPost ) serviceApplicationContext.getBean(req.getRequestURI()+"POST");

        resp.setContentType ("application/json; charset=UTF-8");

        try {
            om.writeValue(resp.getWriter(),
                    controller.handle(
                            om.readValue(req.getInputStream(), controller.getRequestClass()),
                            serviceApplicationContext));
        } catch (Exception e) {
            try {
                e.printStackTrace();
                om.writeValue(resp.getWriter(),
                        new ErrorResponse(e.getMessage()));
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }

    }
}

package ru.alexandrov.webview;


import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.ApplicationContext;
import ru.alexandrov.service.dto.UserDTO;
import ru.alexandrov.webview.json.ControllerGet;
import ru.alexandrov.webview.json.ControllerPost;
import ru.alexandrov.webview.json.response.ErrorResponse;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class MainServlet extends HttpServlet {
    private ObjectMapper om = new ObjectMapper();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        UserDTO authorizedUser = (UserDTO) req.getSession().getAttribute("authorizedUser");
        if (authorizedUser == null) resp.sendRedirect("/authorization");

        ApplicationContext serviceApplicationContext = ( ApplicationContext ) req.getServletContext().getAttribute("springContext");
        String URL;
        if (req.getRequestURI().equals("/")){
            URL = "/accountGET";
        } else {
            URL = req.getRequestURI()+"GET";
        }
        ControllerGet controller = ( ControllerGet ) serviceApplicationContext.getBean(URL);

        resp.setContentType ("application/json; charset=UTF-8");

        try {
            om.writeValue(resp.getWriter(),
                    controller.handle(authorizedUser, req));
        } catch (Exception e) {
            e.printStackTrace();
            om.writeValue(resp.getWriter(),
                    new ErrorResponse(e.getMessage()));
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        ApplicationContext serviceApplicationContext = ( ApplicationContext ) req.getServletContext().getAttribute("springContext");
        ControllerPost controller = ( ControllerPost ) serviceApplicationContext.getBean(req.getRequestURI()+"POST");

        resp.setContentType ("application/json; charset=UTF-8");

        try {
            om.writeValue(resp.getWriter(),
                    controller.handle(
                            om.readValue(req.getInputStream(), controller.getRequestClass()),
                            serviceApplicationContext));
        } catch (Exception e) {
            e.printStackTrace();
            om.writeValue(resp.getWriter(),
                    new ErrorResponse(e.getMessage()));
        }
    }
}

package ru.alexandrov.webview;

import org.springframework.context.ApplicationContext;
import ru.alexandrov.service.AuthorizationService;
import ru.alexandrov.service.dto.UserDTO;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static java.util.Objects.nonNull;

public class AuthorizationServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        final String login = req.getParameter("login");
        final String password = req.getParameter("password");

        ApplicationContext serviceApplicationContext = (ApplicationContext) req.getServletContext().getAttribute("springContext");
        AuthorizationService authService = (AuthorizationService) serviceApplicationContext.getBean("AuthorizationService");
        UserDTO authorizedUser = authService.authorization(login, password);
        final HttpSession session = req.getSession();
        if ((nonNull(session) &&
                nonNull(session.getAttribute("login")) &&
                nonNull(session.getAttribute("password"))) ||
                authorizedUser!= null) {
            req.getSession().setAttribute("authorizedUser", authorizedUser);
            resp.sendRedirect("/account");
        } else {
            resp.sendRedirect("/403");
        }
    }
}

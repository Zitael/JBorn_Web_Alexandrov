package ru.alexandrov.webview;

import ru.alexandrov.service.BankAccountService;
import ru.alexandrov.service.dto.BankAccountDTO;
import ru.alexandrov.service.dto.UserDTO;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

public class GetBankInfoServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        UserDTO authorizedUser = (UserDTO) req.getSession().getAttribute("authorizedUser");
        resp.setContentType ("application/json; charset=UTF-8");
        PrintWriter writer = resp.getWriter();
        writer.println("Hello, " + authorizedUser.getName());
        writer.println();

        BankAccountService bankAccountService = (BankAccountService) req.getServletContext().getAttribute("bankAccountService");
        ArrayList<BankAccountDTO> bankAccountList = bankAccountService.findBankAccountsByUserId(authorizedUser.getId());
        for (BankAccountDTO bankAccountDTO : bankAccountList) {
            writer.println(bankAccountDTO.getAccountName());
            writer.println(bankAccountDTO.getAmount());
        }
    }
}

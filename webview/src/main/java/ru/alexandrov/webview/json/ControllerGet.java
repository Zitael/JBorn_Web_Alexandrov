package ru.alexandrov.webview.json;

import lombok.Getter;
import ru.alexandrov.service.dto.UserDTO;

import javax.servlet.http.HttpServletRequest;

@Getter
public abstract class ControllerGet<RES> {
    public abstract RES handle(UserDTO authUser, HttpServletRequest request);
}

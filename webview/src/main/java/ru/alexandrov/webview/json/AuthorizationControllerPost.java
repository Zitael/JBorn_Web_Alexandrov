package ru.alexandrov.webview.json;

import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import ru.alexandrov.dao.UsersDAO;
import ru.alexandrov.dao.entity.Users;
import ru.alexandrov.webview.json.request.RegistrationRequest;
import ru.alexandrov.webview.json.response.UserResponse;

@Service("/registrationPOST")
public class AuthorizationControllerPost extends ControllerPost<RegistrationRequest,UserResponse> {
    protected AuthorizationControllerPost() {
        super(RegistrationRequest.class);
    }

    @Override
    public UserResponse handle(RegistrationRequest request, ApplicationContext serviceApplicationContext) {
        UsersDAO usersDAO = serviceApplicationContext.getBean(UsersDAO.class);
        Users users = usersDAO.insert(new Users()
                .setLogin(request.getLogin())
                .setName(request.getName())
                .setPassword(request.getPassword()));
        if (users != null) return new UserResponse().setLogin(users.getLogin()).setName(users.getName());
        return null;
    }
}

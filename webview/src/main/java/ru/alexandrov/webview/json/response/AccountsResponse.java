package ru.alexandrov.webview.json.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import ru.alexandrov.service.dto.BankAccountDTO;
import java.util.ArrayList;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class AccountsResponse {
    private String name;
    private ArrayList<BankAccountDTO> bankAccountList;
}

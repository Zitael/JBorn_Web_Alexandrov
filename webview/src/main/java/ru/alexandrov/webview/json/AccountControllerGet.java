package ru.alexandrov.webview.json;

import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import ru.alexandrov.service.BankAccountService;
import ru.alexandrov.service.dto.UserDTO;
import ru.alexandrov.webview.json.response.AccountsResponse;

import javax.servlet.http.HttpServletRequest;

@Service("/accountGET")
public class AccountControllerGet extends ControllerGet<AccountsResponse> {
    @Override
    public AccountsResponse handle(UserDTO authUser, HttpServletRequest req) {
        ApplicationContext serviceApplicationContext = (ApplicationContext) req.getServletContext().getAttribute("springContext");
        BankAccountService bankAccountService = serviceApplicationContext.getBean(BankAccountService.class);
        return new AccountsResponse()
                .setName(authUser.getName())
                .setBankAccountList(bankAccountService.findBankAccountsByUserId(authUser.getId()));
    }
}

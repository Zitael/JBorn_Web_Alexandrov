package ru.alexandrov.webview.json;

import lombok.Getter;
import org.springframework.context.ApplicationContext;

@Getter
public abstract class ControllerPost<REQ, RES> {
    private final Class<REQ> requestClass;

    protected ControllerPost(Class<REQ> requestClass) {
        this.requestClass = requestClass;
    }

    public abstract RES handle(REQ request, ApplicationContext context);
}

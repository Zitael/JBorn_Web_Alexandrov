package ru.alexandrov.webview.json;

import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import ru.alexandrov.webview.json.request.AuthRequest;
import ru.alexandrov.webview.json.response.AccountsResponse;

@Service("/accountPOST")
public class AccountControllerPost extends ControllerPost<AuthRequest, AccountsResponse> {
    protected AccountControllerPost() {
        super(AuthRequest.class);
    }

    @Override
    public AccountsResponse handle(AuthRequest request, ApplicationContext context) {
        return null;
    }
}

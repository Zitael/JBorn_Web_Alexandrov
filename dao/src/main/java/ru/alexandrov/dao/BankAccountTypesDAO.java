package ru.alexandrov.dao;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import ru.alexandrov.dao.entity.BankAccountTypes;

import javax.persistence.EntityManager;

@Service
public class BankAccountTypesDAO extends AbstractDao<BankAccountTypes, Integer> {

    public BankAccountTypesDAO(@Qualifier("createEntityManage") EntityManager em) {
        super(em, BankAccountTypes.class);
    }
}


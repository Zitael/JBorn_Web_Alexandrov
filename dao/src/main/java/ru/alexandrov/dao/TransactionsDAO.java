package ru.alexandrov.dao;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import ru.alexandrov.dao.entity.Transactions;

import javax.persistence.EntityManager;

@Service
public class TransactionsDAO extends AbstractDao<Transactions, Integer> {

    public TransactionsDAO(@Qualifier("createEntityManage") EntityManager em) {
        super(em, Transactions.class);
    }
}

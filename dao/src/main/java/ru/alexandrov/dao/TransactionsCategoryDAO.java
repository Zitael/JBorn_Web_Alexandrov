package ru.alexandrov.dao;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import ru.alexandrov.dao.entity.TransactionsCategory;

import javax.persistence.EntityManager;

@Service
public class TransactionsCategoryDAO extends AbstractDao<TransactionsCategory, Integer> {

    public TransactionsCategoryDAO(@Qualifier("createEntityManage") EntityManager em) {
        super(em, TransactionsCategory.class);
    }
}

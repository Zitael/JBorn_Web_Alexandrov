package ru.alexandrov.dao;

import lombok.Getter;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.util.List;

public abstract class AbstractDao<DOMAIN, ID> {
    @Getter
    private EntityManager em;
    private Class<DOMAIN> objectClass;

    protected AbstractDao(EntityManager em, Class<DOMAIN> objectClass){
        this.em = em;
        this.objectClass = objectClass;
    }
    public List findAll() {
        return em.createNamedQuery(objectClass.getSimpleName() + ".findAll")
                .getResultList();
    }

    DOMAIN findById(ID id){
        return em.find(objectClass, id);
    }

    DOMAIN insert(DOMAIN domain){
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        em.persist(domain);
        tx.commit();
        return domain;
    }

    void update(DOMAIN domain){
        em.merge(domain);
    }

    void delete(ID id){
        DOMAIN domain = em.find(objectClass, id);
        em.remove(domain);
    }
}

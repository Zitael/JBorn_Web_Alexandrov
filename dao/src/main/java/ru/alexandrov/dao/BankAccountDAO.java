package ru.alexandrov.dao;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import ru.alexandrov.dao.entity.BankAccount;

import javax.persistence.EntityManager;
import java.util.List;

@Service
public class BankAccountDAO extends AbstractDao<BankAccount, Integer> {

    public BankAccountDAO(@Qualifier("createEntityManage") EntityManager em) {
        super(em, BankAccount.class);
    }

    public List findByUserId(Integer id) {
        return super.getEm().createNamedQuery("BankAccount.findByUserId")
                .setParameter("userId", id)
                .getResultList();
    }
}
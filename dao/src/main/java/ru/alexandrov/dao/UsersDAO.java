package ru.alexandrov.dao;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import ru.alexandrov.dao.entity.Users;

import javax.persistence.EntityManager;
import java.util.List;

@Service
public class UsersDAO extends AbstractDao<Users, Integer> {
    private EntityManager em;

    public UsersDAO(@Qualifier("createEntityManage") EntityManager em) {
        super(em, Users.class);
    }

    public Users findByEmail(String email) {
        List list = super.getEm().createNamedQuery("Users.findByEmail")
                .setParameter("userEmail", email)
                .getResultList();
        for (Object o : list) {
            if (o instanceof Users) return ( Users ) o;
        }
        return null;
    }
}

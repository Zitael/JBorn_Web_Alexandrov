package ru.alexandrov.dao;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import ru.alexandrov.dao.entity.TransactionsTypes;

import javax.persistence.EntityManager;

@Service
public class TransactionsTypesDAO extends AbstractDao<TransactionsTypes, Integer> {

    public TransactionsTypesDAO(@Qualifier("createEntityManage") EntityManager em) {
        super(em, TransactionsTypes.class);
    }
}

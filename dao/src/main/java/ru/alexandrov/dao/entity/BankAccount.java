package ru.alexandrov.dao.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.math.BigDecimal;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@Entity
@Table(name = "bank_account")
@NamedQueries({
        @NamedQuery(name = "BankAccount.findByUserId", query = "select a from BankAccount a where a.users.id = :userId"),
        @NamedQuery(name = "BankAccount.findAll", query = "select a from BankAccount a")
})
public class BankAccount {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "amount")
    private BigDecimal amount;

    @Column(name = "account_name")
    private String accountName;

    @ManyToOne(targetEntity = Users.class, fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private Users users;

    @ManyToOne(targetEntity =  BankAccountTypes.class, fetch = FetchType.LAZY)
    @JoinColumn(name = "account_type_id")
    private BankAccountTypes bankAccountTypes;
}

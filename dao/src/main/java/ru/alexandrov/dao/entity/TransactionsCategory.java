package ru.alexandrov.dao.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.persistence.*;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@Entity
@Table(name = "trans_category")
@NamedQueries({
        @NamedQuery(name = "TransactionsCategory.findAll", query = "select a from TransactionsCategory a")
})
public class TransactionsCategory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "category_name")
    private String categoryName;
}

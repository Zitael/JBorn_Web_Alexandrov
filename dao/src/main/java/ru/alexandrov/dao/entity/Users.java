package ru.alexandrov.dao.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.persistence.*;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@Entity
@Table(name = "users")
@NamedQueries({
        @NamedQuery(name = "Users.findByEmail", query = "select a from Users a where a.login = :userEmail"),
        @NamedQuery(name = "Users.findAll", query = "select a from Users a")
})
public class Users {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "login")
    private String login;

    @Column(name = "password")
    private String password;

    @Column(name = "name")
    private String name;
}

package ru.alexandrov.dao.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.persistence.*;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@Entity
@Table(name = "trans_types")
@NamedQueries({
        @NamedQuery(name = "TransactionsTypes.findAll", query = "select a from TransactionsTypes a")
})
public class TransactionsTypes {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "transaction_type")
    private String transactionType;

    @ManyToOne(targetEntity = TransactionsCategory.class, fetch = FetchType.LAZY)
    @JoinColumn(name = "transaction_category_id")
    private TransactionsCategory transactionsCategory;
}

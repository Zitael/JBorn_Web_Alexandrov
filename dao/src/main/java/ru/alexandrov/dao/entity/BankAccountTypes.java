package ru.alexandrov.dao.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.persistence.*;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@Entity
@Table(name = "bank_account_types")
@NamedQueries({
        @NamedQuery(name = "BankAccountTypes.findAll", query = "select a from BankAccountTypes a")
})
public class BankAccountTypes {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "account_type_name")
    private String accountTypeName;
}

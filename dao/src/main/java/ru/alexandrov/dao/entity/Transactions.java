package ru.alexandrov.dao.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.math.BigDecimal;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@Entity
@Table(name = "transactions")
@NamedQueries({
        @NamedQuery(name = "Transactions.findAll", query = "select a from Transactions a")
})
public class Transactions {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Version
    private long version;

    @ManyToOne(targetEntity = Users.class, fetch = FetchType.LAZY)
    @JoinColumn
    private Users users;

    @ManyToOne(targetEntity = TransactionsTypes.class, fetch = FetchType.LAZY)
    @JoinColumn(name = "transaction_type_id")
    private TransactionsTypes transactionsTypes;

    @Column(name = "transaction_amount")
    private BigDecimal transactionAmount;
}

package ru.alexandrov.dao;

public class MyException extends RuntimeException{
    public MyException(String message, Throwable t){
        super(message, t);
    }
}

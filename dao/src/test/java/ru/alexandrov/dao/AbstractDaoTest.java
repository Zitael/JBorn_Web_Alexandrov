package ru.alexandrov.dao;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.alexandrov.dao.entity.BankAccount;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class AbstractDaoTest {
    private BankAccountDAO subj;

    @Before
    public void setUp() throws Exception {
        System.setProperty("jdbcUrl", "jdbc:h2:mem:test_mem");
        System.setProperty("username", "sa");
        System.setProperty("password", "");
        System.setProperty("databaseChangeLog", "databaseChangeLog_test.xml");
        ApplicationContext context = new AnnotationConfigApplicationContext(JpaConfiguration.class);
        subj = context.getBean(BankAccountDAO.class);
    }

    @Test
    public void findByIdWithSuccess() {
        BankAccount bankAccount = subj.findById(1);
        assertEquals(BigDecimal.valueOf(1000), bankAccount.getAmount());
        assertEquals("Кошелек", bankAccount.getAccountName());
        assertEquals(1, bankAccount.getUsers().getId());
        assertEquals(1, bankAccount.getBankAccountTypes().getId());
    }
    @Test
    public void findByIdWithNotFoundId() {
        BankAccount bankAccount = subj.findById(1000);
        assertNull(bankAccount);
    }

    @Test
    public void insert() {
        BankAccount bankAccount = new BankAccount();
        bankAccount.setAccountName("Кредитка");
        bankAccount.setBankAccountTypes(null);
        bankAccount.setAmount(BigDecimal.valueOf(1000));
        BankAccount bankAccount1 = subj.insert(bankAccount);
        assertNotNull(bankAccount1);
    }

    @Test
    public void update() {
        BankAccount bankAccount = new BankAccount();
        bankAccount.setAccountName("Кредитка");
        bankAccount.setBankAccountTypes(null);
        bankAccount.setAmount(BigDecimal.valueOf(1000));
        bankAccount.setUsers(null);
        bankAccount.setId(2);
        subj.update(bankAccount);
        BankAccount bankAccount1 = subj.findById(2);
        assertEquals("Кредитка", bankAccount1.getAccountName());
    }

    @Test
    public void delete() {
        subj.delete(3);
        BankAccount bankAccount = subj.findById(3);
        assertNull(bankAccount);
    }

    /*@Test
    public void findAll() {
    }

    @Test
    public void findById() {
    }

    @Test
    public void insert() {
    }

    @Test
    public void update() {
    }

    @Test
    public void delete() {
    }*/
}
package ru.alexandrov.dao;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.alexandrov.dao.entity.Users;

import static org.junit.Assert.assertNotNull;

public class UsersDAOTest {
    private UsersDAO subj;

    @Before
    public void setUp() throws Exception {
        System.setProperty("jdbcUrl", "jdbc:h2:mem:test_mem");
        System.setProperty("username", "sa");
        System.setProperty("password", "");
        System.setProperty("databaseChangeLog", "databaseChangeLog_test.xml");
        ApplicationContext context = new AnnotationConfigApplicationContext(JpaConfiguration.class);
        subj = context.getBean(UsersDAO.class);
    }

    @Test
    public void findByEmailWithSuccess() {
        Users users = subj.findByEmail("maximlogin");
        assertNotNull(users);
    }
}